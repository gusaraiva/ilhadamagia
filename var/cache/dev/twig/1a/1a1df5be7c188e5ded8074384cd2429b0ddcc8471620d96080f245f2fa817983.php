<?php

/* FrontBundle::Front/index.html.twig */
class __TwigTemplate_f1689c441028bea85902283ccbb4a19dffdde34e80c842a3bf956f3039a3a7e0 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        // line 1
        $this->parent = $this->loadTemplate("FrontBundle::Layout/base.html.twig", "FrontBundle::Front/index.html.twig", 1);
        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body_class' => [$this, 'block_body_class'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        return "FrontBundle::Layout/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::Front/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::Front/index.html.twig"));

        // line 3
        $context["menu_active"] = "home";
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ilha da Magia - Descubra Floripa"), "html", null, true);
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 7
    public function block_body_class($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_class"));

        echo "header-transparent";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 10
        echo "    ";
        $this->loadTemplate("FrontBundle::Helper/hero.html.twig", "FrontBundle::Front/index.html.twig", 10)->display($context);
        // line 11
        echo "
    <div class=\"container\">
        <div class=\"content mb-70\">
            ";
        // line 14
        if (twig_length_filter($this->env, (isset($context["locations"]) || array_key_exists("locations", $context) ? $context["locations"] : (function () { throw new Twig_Error_Runtime('Variable "locations" does not exist.', 14, $this->source); })()))) {
            // line 15
            echo "                ";
            $this->loadTemplate("FrontBundle::Helper/page_title.html.twig", "FrontBundle::Front/index.html.twig", 15)->display(array_merge($context, ["title" => "Descubra os melhores lugares", "description" => "Locais mais visitados. Não achou sua cidade? Fique à vontade para nos contar.
"]));
            // line 17
            echo "
                <div class=\"locations-wrapper\">
                    <div class=\"row\">
                        ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["locations"]) || array_key_exists("locations", $context) ? $context["locations"] : (function () { throw new Twig_Error_Runtime('Variable "locations" does not exist.', 20, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["location"]) {
                // line 21
                echo "                            <div class=\"col-md-4\">
                                ";
                // line 22
                $this->loadTemplate("FrontBundle::Location/box.html.twig", "FrontBundle::Front/index.html.twig", 22)->display($context);
                // line 23
                echo "                            </div><!-- /.col-* -->
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['location'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "                    </div><!-- /.row -->
                </div><!-- /.locations-wrapper -->
            ";
        }
        // line 28
        echo "
            ";
        // line 29
        if (twig_length_filter($this->env, (isset($context["listings"]) || array_key_exists("listings", $context) ? $context["listings"] : (function () { throw new Twig_Error_Runtime('Variable "listings" does not exist.', 29, $this->source); })()))) {
            // line 30
            echo "                ";
            $this->loadTemplate("FrontBundle::Helper/page_title.html.twig", "FrontBundle::Front/index.html.twig", 30)->display(array_merge($context, ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Lugares Adicinados Recentemente"), "description" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Descubra os lugares adicionados recentemente")]));
            // line 31
            echo "
                <div class=\"listings-wrapper\">
                    <div class=\"row\">
                        ";
            // line 34
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["listings"]) || array_key_exists("listings", $context) ? $context["listings"] : (function () { throw new Twig_Error_Runtime('Variable "listings" does not exist.', 34, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["listing"]) {
                // line 35
                echo "                            <div class=\"col-md-4\">
                                ";
                // line 36
                $this->loadTemplate("FrontBundle::Listing/box.html.twig", "FrontBundle::Front/index.html.twig", 36)->display($context);
                // line 37
                echo "                            </div><!-- /.col-* -->
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['listing'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                    </div><!-- /.row -->
                </div><!-- /.listings-wrapper -->
            ";
        }
        // line 42
        echo "
            ";
        // line 43
        if (twig_length_filter($this->env, (isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new Twig_Error_Runtime('Variable "categories" does not exist.', 43, $this->source); })()))) {
            // line 44
            echo "                ";
            $this->loadTemplate("FrontBundle::Helper/page_title.html.twig", "FrontBundle::Front/index.html.twig", 44)->display(array_merge($context, ["title" => "Categorias & Interesses", "description" => "Categorias mais populares no sistema ordenadas pelo maior número de listagens atribuídas."]));
            // line 45
            echo "
                <div class=\"categories-wrapper\">
                    <div class=\"row\">
                        ";
            // line 48
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new Twig_Error_Runtime('Variable "categories" does not exist.', 48, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
                // line 49
                echo "                            <div class=\"col-md-3\">
                                ";
                // line 50
                $this->loadTemplate("FrontBundle::Category/box.html.twig", "FrontBundle::Front/index.html.twig", 50)->display($context);
                // line 51
                echo "                            </div><!-- /.col-* -->
                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "                    </div><!-- /.row -->
                </div><!-- /.categories-wrapper -->

                <div class=\"btn-center\">
                    <a href=\"";
            // line 57
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("category");
            echo "\" class=\"btn btn-primary\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mostrar Todas Categorias"), "html", null, true);
            echo "</a>
                </div><!-- /.btn-center -->
            ";
        }
        // line 60
        echo "
            ";
        // line 61
        if (twig_length_filter($this->env, (isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 61, $this->source); })()))) {
            // line 62
            echo "                ";
            $this->loadTemplate("FrontBundle::Helper/page_title.html.twig", "FrontBundle::Front/index.html.twig", 62)->display(array_merge($context, ["title" => "Dicas & Novidades", "description" => "Leia as postagens do blog sobre nossas recomendações e experiências.", "class" => "border-top"]));
            // line 67
            echo "
                <div class=\"posts-wrapper\">
                    <div class=\"row\">
                        <div class=\"col-md-10 push-md-1\">                    
                            ";
            // line 71
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 71, $this->source); })()));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
                echo "                    
                                ";
                // line 72
                $this->loadTemplate("FrontBundle::Post/row.html.twig", "FrontBundle::Front/index.html.twig", 72)->display($context);
                // line 73
                echo "                            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 74
            echo "                        </div><!-- /.col-* -->
                    </div><!-- /.row -->                    
                </div><!-- /.posts-wrapper -->

                <div class=\"btn-center\">
                    <a href=\"";
            // line 79
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("post");
            echo "\" class=\"btn btn-primary\">
                        ";
            // line 80
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ver todos os posts"), "html", null, true);
            echo "
                    </a>
                </div><!-- /.btn-center -->
            ";
        }
        // line 84
        echo "        </div><!-- /.content -->
    </div><!-- /.container -->
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle::Front/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  341 => 84,  334 => 80,  330 => 79,  323 => 74,  309 => 73,  307 => 72,  288 => 71,  282 => 67,  279 => 62,  277 => 61,  274 => 60,  266 => 57,  260 => 53,  245 => 51,  243 => 50,  240 => 49,  223 => 48,  218 => 45,  215 => 44,  213 => 43,  210 => 42,  205 => 39,  190 => 37,  188 => 36,  185 => 35,  168 => 34,  163 => 31,  160 => 30,  158 => 29,  155 => 28,  150 => 25,  135 => 23,  133 => 22,  130 => 21,  113 => 20,  108 => 17,  104 => 15,  102 => 14,  97 => 11,  94 => 10,  85 => 9,  67 => 7,  49 => 5,  39 => 1,  37 => 3,  15 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'FrontBundle::Layout/base.html.twig' %}

{% set menu_active = 'home' %}
    
{% block title %}{{ 'Ilha da Magia - Descubra Floripa'|trans }}{% endblock %}

{% block body_class %}header-transparent{% endblock %}

{% block content %}
    {% include 'FrontBundle::Helper/hero.html.twig' %}

    <div class=\"container\">
        <div class=\"content mb-70\">
            {% if locations|length %}
                {% include 'FrontBundle::Helper/page_title.html.twig' with {title: \"Descubra os melhores lugares\", description: \"Locais mais visitados. Não achou sua cidade? Fique à vontade para nos contar.
\"} %}

                <div class=\"locations-wrapper\">
                    <div class=\"row\">
                        {% for location in locations %}
                            <div class=\"col-md-4\">
                                {% include 'FrontBundle::Location/box.html.twig' %}
                            </div><!-- /.col-* -->
                        {% endfor %}
                    </div><!-- /.row -->
                </div><!-- /.locations-wrapper -->
            {% endif %}

            {% if listings|length %}
                {% include 'FrontBundle::Helper/page_title.html.twig' with {title: \"Lugares Adicinados Recentemente\"|trans, description: \"Descubra os lugares adicionados recentemente\"|trans} %}

                <div class=\"listings-wrapper\">
                    <div class=\"row\">
                        {% for listing in listings %}
                            <div class=\"col-md-4\">
                                {% include 'FrontBundle::Listing/box.html.twig' %}
                            </div><!-- /.col-* -->
                        {% endfor %}
                    </div><!-- /.row -->
                </div><!-- /.listings-wrapper -->
            {% endif %}

            {% if categories|length %}
                {% include 'FrontBundle::Helper/page_title.html.twig' with {title: \"Categorias & Interesses\", description: \"Categorias mais populares no sistema ordenadas pelo maior número de listagens atribuídas.\"} %}

                <div class=\"categories-wrapper\">
                    <div class=\"row\">
                        {% for category in categories %}
                            <div class=\"col-md-3\">
                                {% include 'FrontBundle::Category/box.html.twig' %}
                            </div><!-- /.col-* -->
                        {% endfor %}
                    </div><!-- /.row -->
                </div><!-- /.categories-wrapper -->

                <div class=\"btn-center\">
                    <a href=\"{{ path('category') }}\" class=\"btn btn-primary\">{{ 'Mostrar Todas Categorias'|trans }}</a>
                </div><!-- /.btn-center -->
            {% endif %}

            {% if posts|length %}
                {% include 'FrontBundle::Helper/page_title.html.twig' with {
                    title: \"Dicas & Novidades\",
                    description: \"Leia as postagens do blog sobre nossas recomendações e experiências.\",
                    class: \"border-top\"
                } %}

                <div class=\"posts-wrapper\">
                    <div class=\"row\">
                        <div class=\"col-md-10 push-md-1\">                    
                            {% for post in posts %}                    
                                {% include 'FrontBundle::Post/row.html.twig' %}
                            {% endfor %}
                        </div><!-- /.col-* -->
                    </div><!-- /.row -->                    
                </div><!-- /.posts-wrapper -->

                <div class=\"btn-center\">
                    <a href=\"{{ path('post')}}\" class=\"btn btn-primary\">
                        {{ 'Ver todos os posts'|trans }}
                    </a>
                </div><!-- /.btn-center -->
            {% endif %}
        </div><!-- /.content -->
    </div><!-- /.container -->
{% endblock %}", "FrontBundle::Front/index.html.twig", "/Users/gustavosaraiva1/Desktop/magia/src/DirectoryPlatform/FrontBundle/Resources/views/Front/index.html.twig");
    }
}
