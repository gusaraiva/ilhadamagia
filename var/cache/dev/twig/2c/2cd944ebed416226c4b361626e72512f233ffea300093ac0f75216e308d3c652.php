<?php

/* FrontBundle::Helper/header.html.twig */
class __TwigTemplate_07cae3d6991b776af53331c1e827275da470aacddf9b4488888f53901f2efdd3 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::Helper/header.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::Helper/header.html.twig"));

        // line 1
        echo "<div class=\"header-wrapper\">
    <div class=\"header\">
        <div class=\"container";
        // line 3
        if ((isset($context["fullwidth"]) || array_key_exists("fullwidth", $context))) {
            echo "-fluid";
        }
        echo "\">
            <div class=\"header-brand\">
                <a href=\"";
        // line 5
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\">
                    <span class=\"header-brand-image\">                        
                        <svg width=\"40px\" height=\"50px\" viewBox=\"0 0 40 50\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                            <defs></defs>
                            <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">
                                <g id=\"logo\" transform=\"translate(-20.000000, -15.000000)\" stroke=\"#FF1D47\" stroke-width=\"3\">
                                    <g id=\"Marker\" transform=\"translate(22.000000, 17.000000)\">
                                        <path d=\"M18,45.2332275 C30,33.8305966 36,24.7932582 36,18.1212121 C36,8.11314302 27.9411255,0 18,0 C8.0588745,0 0,8.11314302 0,18.1212121 C0,24.7932582 6,33.8305966 18,45.2332275 Z\" id=\"Oval\"></path>
                                        <ellipse id=\"Oval-2\" cx=\"18\" cy=\"16.7272727\" rx=\"5.53846154\" ry=\"5.57575758\"></ellipse>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        ";
        // line 23
        echo "                    </span><!-- /.header-brand-image -->

                    <span class=\"header-brand-title\">
                        <strong>Ilha da</strong><span> Magia</span>
                    </span><!-- /.header-brand-title -->
                </a>
            </div><!-- /.header-brand -->

            <div class=\"header-nav-primary\">
                <ul class=\"nav\">
                    <li class=\"nav-item\">
                        <a href=\"";
        // line 34
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("homepage");
        echo "\" class=\"nav-link ";
        if (((isset($context["menu_active"]) || array_key_exists("menu_active", $context)) && ((isset($context["menu_active"]) || array_key_exists("menu_active", $context) ? $context["menu_active"] : (function () { throw new Twig_Error_Runtime('Variable "menu_active" does not exist.', 34, $this->source); })()) == "home"))) {
            echo "active";
        }
        echo "\">
                            ";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Home"), "html", null, true);
        echo "
                        </a> 
                    </li><!-- /.nav-item -->

                    <li class=\"nav-item\">
                        <a href=\"";
        // line 40
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listing");
        echo "\" class=\"nav-link ";
        if (((isset($context["menu_active"]) || array_key_exists("menu_active", $context)) && ((isset($context["menu_active"]) || array_key_exists("menu_active", $context) ? $context["menu_active"] : (function () { throw new Twig_Error_Runtime('Variable "menu_active" does not exist.', 40, $this->source); })()) == "listings"))) {
            echo "active";
        }
        echo "\">
                            ";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Descubra"), "html", null, true);
        echo "
                        </a> 
                    </li><!-- /.nav-item -->

                    <li class=\"nav-item\">
                        <a href=\"";
        // line 46
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("post");
        echo "\" class=\"nav-link ";
        if (((isset($context["menu_active"]) || array_key_exists("menu_active", $context)) && ((isset($context["menu_active"]) || array_key_exists("menu_active", $context) ? $context["menu_active"] : (function () { throw new Twig_Error_Runtime('Variable "menu_active" does not exist.', 46, $this->source); })()) == "posts"))) {
            echo "active";
        }
        echo "\">
                            ";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Blog"), "html", null, true);
        echo "
                        </a> 
                    </li><!-- /.nav-item -->                    
                    
                    ";
        // line 66
        echo "                </ul><!-- /.nav -->
            </div><!-- /.header-nav-primary -->

            <div class=\"header-nav-secondary\">
                <ul class=\"nav\">
                    ";
        // line 71
        if ( !twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 71, $this->source); })()), "user", [])) {
            // line 72
            echo "                        <li class=\"nav-item hidden-sm-down\">
                            <a href=\"";
            // line 73
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_login");
            echo "\" class=\"nav-link\" data-toggle=\"modal\" data-target=\"#modal-login\">
                                ";
            // line 74
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Entrar"), "html", null, true);
            echo "
                            </a>
                        </li>

                        ";
            // line 78
            echo twig_escape_filter($this->env, (isset($context["enable_registration"]) || array_key_exists("enable_registration", $context) ? $context["enable_registration"] : (function () { throw new Twig_Error_Runtime('Variable "enable_registration" does not exist.', 78, $this->source); })()), "html", null, true);
            echo "
                        ";
            // line 79
            if ((isset($context["enable_registration"]) || array_key_exists("enable_registration", $context) ? $context["enable_registration"] : (function () { throw new Twig_Error_Runtime('Variable "enable_registration" does not exist.', 79, $this->source); })())) {
                // line 80
                echo "                            <li class=\"nav-item hidden-sm-down\">
                                <a href=\"";
                // line 81
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_registration_register");
                echo "\" class=\"nav-link\" data-toggle=\"modal\" data-target=\"#modal-register\">
                                    ";
                // line 82
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Registrar"), "html", null, true);
                echo "
                                </a>
                            </li>
                        ";
            }
            // line 86
            echo "                    ";
        }
        // line 87
        echo "
                    <li class=\"nav-item\">
                        <a href=\"";
        // line 89
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listing_create");
        echo "\" class=\"nav-link nav-btn\">
                            <i class=\"fa fa-plus\"></i> ";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Adicionar Local"), "html", null, true);
        echo "
                        </a>
                    </li>
                </ul>
            </div><!-- /.header-nav-secondary -->

            ";
        // line 96
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 96, $this->source); })()), "user", [])) {
            // line 97
            echo "                <div class=\"header-nav-user\">
                    <ul class=\"nav\">
                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                ";
            // line 101
            ob_start();
            // line 102
            echo "                                    ";
            if ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 102, $this->source); })()), "user", []), "profile", []) && twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 102, $this->source); })()), "user", []), "profile", []), "getAvatarName", [], "method"))) {
                // line 103
                echo "                                        <img src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 103, $this->source); })()), "user", []), "profile", []), "avatarImage"), "html", null, true);
                echo "\" alt=\"";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 103, $this->source); })()), "user", []), "getProfile", [], "method"), "getDisplayName", [], "method"), "html", null, true);
                echo "\">
                                    ";
            } else {
                // line 105
                echo "                                        <div class=\"dropdown-avatar\">
                                            ";
                // line 106
                if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 106, $this->source); })()), "user", []), "profile", [])) {
                    // line 107
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 107, $this->source); })()), "user", []), "profile", []), "getInitials", [], "method"), "html", null, true);
                    echo "
                                            ";
                } else {
                    // line 109
                    echo "                                                ";
                    echo twig_escape_filter($this->env, twig_first($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 109, $this->source); })()), "user", []), "getUsername", [], "method")), "html", null, true);
                    echo "
                                            ";
                }
                // line 111
                echo "                                        </div><!-- /.dropdown-avatar -->
                                    ";
            }
            // line 113
            echo "
                                    ";
            // line 114
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 114, $this->source); })()), "user", []), "getProfile", [], "method")) {
                // line 115
                echo "                                        <span>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 115, $this->source); })()), "user", []), "getProfile", [], "method"), "getDisplayName", [], "method"), "html", null, true);
                echo "</span>
                                    ";
            } else {
                // line 117
                echo "                                        <span>";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 117, $this->source); })()), "user", []), "getUsername", [], "method"), "html", null, true);
                echo "</span>
                                    ";
            }
            // line 119
            echo "                                ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 120
            echo "                            </a>

                            <div class=\"dropdown-menu\" aria-labelledby=\"header-dropdown\">
                                ";
            // line 123
            if (twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 123, $this->source); })()), "user", []), "hasRole", [0 => "ROLE_SUPER_ADMIN"], "method")) {
                // line 124
                echo "                                    <a class=\"dropdown-item\" href=\"";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_dashboard");
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Painel Administrativo"), "html", null, true);
                echo "</a>
                                ";
            }
            // line 126
            echo "
                                <a class=\"dropdown-item\" href=\"";
            // line 127
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("listing_my");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Locais"), "html", null, true);
            echo "</a>
                                <a class=\"dropdown-item\" href=\"";
            // line 128
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("favorite");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Favoritos"), "html", null, true);
            echo "</a>
                                <a class=\"dropdown-item\" href=\"";
            // line 129
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("search");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Buscas"), "html", null, true);
            echo "</a>
                                <a class=\"dropdown-item\" href=\"";
            // line 130
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("order");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ordens"), "html", null, true);
            echo "</a>                                
                                <a class=\"dropdown-item\" href=\"";
            // line 131
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("profile_update");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Meu perfil"), "html", null, true);
            echo "</a>                                
                                <a class=\"dropdown-item\" href=\"";
            // line 132
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_change_password");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mudar senha"), "html", null, true);
            echo "</a>
                                <a class=\"dropdown-item\" href=\"";
            // line 133
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("fos_user_security_logout");
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Sair"), "html", null, true);
            echo "</a>
                            </div>
                        </li>

                        <li class=\"nav-item shopping-cart\">
                            <a href=\"";
            // line 138
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("cart");
            echo "\" class=\"nav-link\">
                                <i class=\"fa fa-shopping-cart\"></i>

                                <span class=\"shopping-cart-title\">";
            // line 141
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Carrinho"), "html", null, true);
            echo "</span>

                                <span class=\"badge\">";
            // line 143
            echo twig_escape_filter($this->env, $this->extensions['DirectoryPlatform\AppBundle\Twig\CartExtension']->getCount($this->env), "html", null, true);
            echo "</span><!-- /.badge -->
                            </a><!-- /.nav-link -->
                        </li><!-- /.shopping-cart -->
                    </ul><!-- /.nav -->
                </div><!-- /.header-nav-user -->
            ";
        }
        // line 149
        echo "        </div><!-- /.container -->
    </div><!-- /.header -->

    <div class=\"header-toggle\">
        <span>";
        // line 153
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Alternar de navegação"), "html", null, true);
        echo "</span>
    </div><!-- /.header-toggle -->
</div><!-- /.header-wrapper -->";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle::Helper/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  331 => 153,  325 => 149,  316 => 143,  311 => 141,  305 => 138,  295 => 133,  289 => 132,  283 => 131,  277 => 130,  271 => 129,  265 => 128,  259 => 127,  256 => 126,  248 => 124,  246 => 123,  241 => 120,  238 => 119,  232 => 117,  226 => 115,  224 => 114,  221 => 113,  217 => 111,  211 => 109,  205 => 107,  203 => 106,  200 => 105,  192 => 103,  189 => 102,  187 => 101,  181 => 97,  179 => 96,  170 => 90,  166 => 89,  162 => 87,  159 => 86,  152 => 82,  148 => 81,  145 => 80,  143 => 79,  139 => 78,  132 => 74,  128 => 73,  125 => 72,  123 => 71,  116 => 66,  109 => 47,  101 => 46,  93 => 41,  85 => 40,  77 => 35,  69 => 34,  56 => 23,  40 => 5,  33 => 3,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"header-wrapper\">
    <div class=\"header\">
        <div class=\"container{% if fullwidth is defined %}-fluid{% endif %}\">
            <div class=\"header-brand\">
                <a href=\"{{ path('homepage') }}\">
                    <span class=\"header-brand-image\">                        
                        <svg width=\"40px\" height=\"50px\" viewBox=\"0 0 40 50\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">
                            <defs></defs>
                            <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\">
                                <g id=\"logo\" transform=\"translate(-20.000000, -15.000000)\" stroke=\"#FF1D47\" stroke-width=\"3\">
                                    <g id=\"Marker\" transform=\"translate(22.000000, 17.000000)\">
                                        <path d=\"M18,45.2332275 C30,33.8305966 36,24.7932582 36,18.1212121 C36,8.11314302 27.9411255,0 18,0 C8.0588745,0 0,8.11314302 0,18.1212121 C0,24.7932582 6,33.8305966 18,45.2332275 Z\" id=\"Oval\"></path>
                                        <ellipse id=\"Oval-2\" cx=\"18\" cy=\"16.7272727\" rx=\"5.53846154\" ry=\"5.57575758\"></ellipse>
                                    </g>
                                </g>
                            </g>
                        </svg>
                        {#
                        Your logo

                        <img src=\"{{ asset('bundles/front/img/logo.svg') }}\" alt=\"{{ 'Home'|trans }}\" class=\"image-svg\">
                        #}
                    </span><!-- /.header-brand-image -->

                    <span class=\"header-brand-title\">
                        <strong>Ilha da</strong><span> Magia</span>
                    </span><!-- /.header-brand-title -->
                </a>
            </div><!-- /.header-brand -->

            <div class=\"header-nav-primary\">
                <ul class=\"nav\">
                    <li class=\"nav-item\">
                        <a href=\"{{ path('homepage') }}\" class=\"nav-link {% if menu_active is defined and menu_active == 'home' %}active{% endif %}\">
                            {{ 'Home'|trans }}
                        </a> 
                    </li><!-- /.nav-item -->

                    <li class=\"nav-item\">
                        <a href=\"{{ path('listing') }}\" class=\"nav-link {% if menu_active is defined and menu_active == 'listings' %}active{% endif %}\">
                            {{ 'Descubra'|trans }}
                        </a> 
                    </li><!-- /.nav-item -->

                    <li class=\"nav-item\">
                        <a href=\"{{ path('post') }}\" class=\"nav-link {% if menu_active is defined and menu_active == 'posts' %}active{% endif %}\">
                            {{ 'Blog'|trans }}
                        </a> 
                    </li><!-- /.nav-item -->                    
                    
                    {#
                    <li class=\"nav-item dropdown\">
                        <a href=\"{{ path('listing') }}\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                            {{ 'Locations'|trans }}
                        </a> 

                        <div class=\"dropdown-menu\">
                            <a href=\"#\" class=\"dropdown-item\">Item 1</a>
                            <a href=\"#\" class=\"dropdown-item\">Item 2</a>
                            <a href=\"#\" class=\"dropdown-item\">Item 3</a>
                            <a href=\"#\" class=\"dropdown-item\">Item 4</a>
                            <a href=\"#\" class=\"dropdown-item\">Item 5</a>
                        </div><!-- /.sub-menu -->
                    </li><!-- /.nav-item -->                    
                    #}
                </ul><!-- /.nav -->
            </div><!-- /.header-nav-primary -->

            <div class=\"header-nav-secondary\">
                <ul class=\"nav\">
                    {% if not app.user %}
                        <li class=\"nav-item hidden-sm-down\">
                            <a href=\"{{ path('fos_user_security_login') }}\" class=\"nav-link\" data-toggle=\"modal\" data-target=\"#modal-login\">
                                {{ 'Entrar'|trans }}
                            </a>
                        </li>

                        {{ enable_registration }}
                        {% if enable_registration %}
                            <li class=\"nav-item hidden-sm-down\">
                                <a href=\"{{ path('fos_user_registration_register') }}\" class=\"nav-link\" data-toggle=\"modal\" data-target=\"#modal-register\">
                                    {{ 'Registrar'|trans }}
                                </a>
                            </li>
                        {% endif %}
                    {% endif %}

                    <li class=\"nav-item\">
                        <a href=\"{{ path('listing_create')}}\" class=\"nav-link nav-btn\">
                            <i class=\"fa fa-plus\"></i> {{ 'Adicionar Local'|trans }}
                        </a>
                    </li>
                </ul>
            </div><!-- /.header-nav-secondary -->

            {% if app.user %}
                <div class=\"header-nav-user\">
                    <ul class=\"nav\">
                        <li class=\"nav-item dropdown\">
                            <a class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                {% spaceless %}
                                    {% if app.user.profile and app.user.profile.getAvatarName() %}
                                        <img src=\"{{ vich_uploader_asset(app.user.profile, 'avatarImage') }}\" alt=\"{{ app.user.getProfile().getDisplayName() }}\">
                                    {% else %}
                                        <div class=\"dropdown-avatar\">
                                            {% if app.user.profile %}
                                                {{ app.user.profile.getInitials() }}
                                            {% else %}
                                                {{ app.user.getUsername()|first }}
                                            {% endif %}
                                        </div><!-- /.dropdown-avatar -->
                                    {% endif %}

                                    {% if app.user.getProfile() %}
                                        <span>{{ app.user.getProfile().getDisplayName() }}</span>
                                    {% else %}
                                        <span>{{ app.user.getUsername() }}</span>
                                    {% endif %}
                                {% endspaceless %}
                            </a>

                            <div class=\"dropdown-menu\" aria-labelledby=\"header-dropdown\">
                                {% if app.user.hasRole('ROLE_SUPER_ADMIN') %}
                                    <a class=\"dropdown-item\" href=\"{{ path('admin_dashboard') }}\">{{ 'Painel Administrativo'|trans }}</a>
                                {% endif %}

                                <a class=\"dropdown-item\" href=\"{{ path('listing_my') }}\">{{ 'Locais'|trans }}</a>
                                <a class=\"dropdown-item\" href=\"{{ path('favorite') }}\">{{ 'Favoritos'|trans }}</a>
                                <a class=\"dropdown-item\" href=\"{{ path('search') }}\">{{ 'Buscas'|trans }}</a>
                                <a class=\"dropdown-item\" href=\"{{ path('order') }}\">{{ 'Ordens'|trans }}</a>                                
                                <a class=\"dropdown-item\" href=\"{{ path('profile_update') }}\">{{ 'Meu perfil'|trans }}</a>                                
                                <a class=\"dropdown-item\" href=\"{{ path('fos_user_change_password') }}\">{{ 'Mudar senha'|trans }}</a>
                                <a class=\"dropdown-item\" href=\"{{ path('fos_user_security_logout') }}\">{{ 'Sair'|trans }}</a>
                            </div>
                        </li>

                        <li class=\"nav-item shopping-cart\">
                            <a href=\"{{ path('cart') }}\" class=\"nav-link\">
                                <i class=\"fa fa-shopping-cart\"></i>

                                <span class=\"shopping-cart-title\">{{ 'Carrinho'|trans }}</span>

                                <span class=\"badge\">{{ cart_get_count() }}</span><!-- /.badge -->
                            </a><!-- /.nav-link -->
                        </li><!-- /.shopping-cart -->
                    </ul><!-- /.nav -->
                </div><!-- /.header-nav-user -->
            {% endif %}
        </div><!-- /.container -->
    </div><!-- /.header -->

    <div class=\"header-toggle\">
        <span>{{ 'Alternar de navegação'|trans }}</span>
    </div><!-- /.header-toggle -->
</div><!-- /.header-wrapper -->", "FrontBundle::Helper/header.html.twig", "/Users/gustavosaraiva1/Desktop/magia/src/DirectoryPlatform/FrontBundle/Resources/views/Helper/header.html.twig");
    }
}
